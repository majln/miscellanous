" Autocompletion for php
set omnifunc=phpcomplete#CompletePHP

" Comment
noremap <C-k> <Esc>:s/^/\/\//<Esc>:nohl<CR>
vnoremap <C-k> :s/^/\/\//<Esc>:nohl<CR>

" Uncomment
noremap <C-l> <Esc>:s/^\/\///<Esc>:nohl<CR>
vnoremap <C-l> :s/^\/\///<Esc>:nohl<CR>
