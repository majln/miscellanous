" Milan Verespej, .vimrc
" Created: 25 February 2017

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" Set filetype-plugin on to detect settings from files with settings
filetype plugin on

" Enable syntax highlighting
syntax on

" Better command-line completion
set wildmenu

" Show partial commands in the last line of the screen
set showcmd

" Always display the status line, even if only one window is displayed
set laststatus=2

" Show line numbers
set number

" Highlighting matching brackets
set showmatch

" How many tenths of second to show when matching
set mat=2

" Highlight search
set hlsearch

" Map <C-h> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-h> :nohl<CR><C-h>

""""""""""""""""""""""""""
" Tabs and Indenting 
""""""""""""""""""""""""""
" Spaces insted of tabs
set expandtab

" 1 tab -> 4 spaces
set shiftwidth=4
set tabstop=4

set ai
set si

" Remapping indenting to tab and shift+tab
noremap <TAB> >
noremap <S-TAB> <

"""""""""""""""""""""""""
" Bracket Handling
"""""""""""""""""""""""""
" Insert closing brackets and quotation marks
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap < <><Esc>i
inoremap " ""<Esc>i
inoremap ' ''<Esc>i

" Curly braces as block of code
inoremap {<CR> {<CR>}<Esc>O

" Escape braces and quotation marks
inoremap <C-j> <Esc>/[)}"'\]>]<CR>:nohl<CR>a

" Surround selected text
vnoremap ( <Esc>`>a)<Esc>`<i(<Esc>
vnoremap [ <Esc>`>a]<Esc>`<i[<Esc>
vnoremap { <Esc>`>a}<Esc>`<i{<Esc>
vnoremap < <Esc>`>a><Esc>`<i<<Esc>
vnoremap " <Esc>`>a"<Esc>`<i"<Esc>
vnoremap ' <Esc>`>a'<Esc>`<i'<Esc>

vnoremap ) <Esc>`>a)<Esc>`<i(<Esc>
vnoremap ] <Esc>`>a]<Esc>`<i[<Esc>
vnoremap } <Esc>`>a}<Esc>`<i{<Esc>
vnoremap > <Esc>`>a><Esc>`<i<<Esc>


